import React from "react"
import Title from "./Title"
import { useTranslation } from "react-i18next"

interface OrdersProps {
  handleDecrementClick: () => void
  handleIncrementClick: () => void
  count: number
}

const Orders = ({ handleDecrementClick, handleIncrementClick, count }: OrdersProps) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <Title>NFTs availables</Title>
      <h1>{t("welcome")}</h1>
      <button onClick={handleDecrementClick}>Decrement</button>
      {count}
      <button onClick={handleIncrementClick}>Increment</button>
    </React.Fragment>
  )
}

export default Orders
