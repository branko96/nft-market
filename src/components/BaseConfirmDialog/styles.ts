import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"

export const useBaseConfirmDialogStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      justifyContent: "center"
    },
    submit: {
      marginRight: theme.spacing(1)
    }
  })
)

export default useBaseConfirmDialogStyles
