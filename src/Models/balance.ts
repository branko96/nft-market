import { OwnerDataType } from "./asset"

export interface BalanceModel {
  address: string
  quantity: number
  asset: string
  description?: string
  supply: number
  assetLongname: string | null
  divisible: boolean
  issuer: string
  locked: boolean
  owner: string
  user?: OwnerDataType
  price?: string
  isSalable?: boolean
  salableAssetId?: number
  assetName?: string
}
