export const FIREBASE_SIGN_UP_ERROR_CODES: Record<string, any> = {
  EMAIL_IN_USE: "auth/email-already-in-use",
  INVALID_EMAIL: "auth/invalid-email",
  WEAK_PASSWORD: "auth/weak-password",
  OPERATION_NOT_ALLOWED: "auth/operation-not-allowed"
}

export const FIREBASE_RESET_PASSWORD_ERROR_CODES: Record<string, any> = {
  USER_NOT_FOUND: "auth/user-not-found"
}

export const AUTH_CONFIG: Record<string, any> = {
  auth: {
    username: process.env.REACT_APP_RPC_USER,
    password: process.env.REACT_APP_RPC_PASSWORD
  }
}

export const ASPIRE_API_METHODS = {
  GET_ASSETS_INFO: "get_asset_info",
  GET_ASSETS: "get_assets",
  GET_BALANCES: "get_balances",
  CREATE_ISSUANCE: "create_issuance",
  GET_ISSUANCES: "get_issuances",
  CREATE_SEND: "create_send"
}

export const NEW_ASSET_ERRORS = {
  INSUFFICIENT_FUNDS: "INSUFICIENT_FUNDS"
}

export const BUY_ASSET_ERRORS = {
  INSUFFICIENT_FUNDS: "INSUFICIENT_FUNDS",
  INTERNAL_SERVER_ERROR: "INTERNAL_SERVER_ERROR",
  BALANCE_EXHAUSTED: "Ups.. the asset balance is exhausted"
}

export const NEW_ASSET_ERROR_CODES_ASPIRE = {
  INSUFFICIENT_FUNDS: -32001
}

interface RpcData {
  method: string
  params: Record<string, any>
  jsonrpc: string
  id: number
}

export const getRpcData: (method, params) => RpcData = (method, params) => {
  return {
    method,
    params,
    jsonrpc: "2.0",
    id: 0
  }
}

export const IPFS_BASE_URL = "https://ipfs.io/ipfs/"

export const getIpfsAssetJson = (url) => {
  return fetch(`${IPFS_BASE_URL}${url}`).then((response) => response.json())
}

export const validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".mp4"]
export const imageFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"]
export const videoFileExtensions = [".mp4"]

export const validateFileExtension = (sFileName: string): boolean => {
  let valid = false
  for (let j = 0; j < validFileExtensions.length; j++) {
    const sCurExtension = validFileExtensions[j]
    const fileExtension = `.${sFileName.split(".").pop()}`

    if (fileExtension.toLowerCase() == sCurExtension.toLowerCase()) {
      valid = true
      break
    }
  }
  return valid
}

export const getIsVideoFromFileExtension = (fileName: string): boolean => {
  let video = false
  for (let j = 0; j < videoFileExtensions.length; j++) {
    const sCurExtension = videoFileExtensions[j]
    const fileExtension = `.${fileName.split(".").pop()}`

    if (fileExtension.toLowerCase() == sCurExtension.toLowerCase()) {
      video = true
      break
    }
  }
  return video
}

export const uniq = (a) => {
  const prims = { boolean: {}, number: {}, string: {} },
    objs = []

  return a.filter(function (item) {
    const type = typeof item
    if (type in prims) return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true)
    else return objs.indexOf(item) >= 0 ? false : objs.push(item)
  })
}

export const PAYMENT_METHODS_ENUM = {
  GASP: "GASP",
  ASP: "ASP",
  ENERCHI: "ENERCHI"
}

export type CoinBalanceType = "GASP" | "ASP" | "ENERCHI"

export const ASPIRE_COIN_ASSETS: Record<CoinBalanceType, any> = {
  GASP: "GASP",
  ASP: "ASP",
  ENERCHI: "ENERCHI"
}

export const MARKET_ASPIRE_COIN_ASSETS: Record<string, any> = {
  GASP: "gasp",
  ASP: "aspire"
}

export const MAX_DURATION_OF_FILE = 90
