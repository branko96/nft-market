import authReducer from "./auth"
import {
  loginStart,
  loginSuccess,
  logout,
  logoutFailed,
  logoutSuccess,
  signUpFailed,
  signUpStart,
  signUpSuccess
} from "../actions/auth"

describe("Auth Duck", () => {
  describe("Action creators", () => {
    it("loginStart", () => {
      const email = "branko-96@hotmail.com"
      const password = "passw"
      const callback = () => null
      const payload = {
        email,
        password,
        callback
      }
      const resultado = loginStart(email, password, callback)
      expect(resultado).toEqual({
        type: "LOGIN_START",
        payload
      })
    })
    it("loginSuccess", () => {
      const user = {
        id: "324432",
        name: "Fede Osov",
        email: "fedosov@gmail.com"
      }
      const resultado = loginSuccess(user)
      expect(resultado).toEqual({
        type: "LOGIN_SUCCESS",
        payload: user
      })
    })
    it("logout", () => {
      const callback = () => null
      const resultado = logout(callback)
      expect(resultado).toEqual({
        type: "LOGOUT",
        payload: {
          callback
        }
      })
    })
    it("logoutSuccess", () => {
      const resultado = logoutSuccess()
      expect(resultado).toEqual({
        type: "LOGOUT_SUCCESS"
      })
    })
    it("logoutFailed", () => {
      const resultado = logoutFailed()
      expect(resultado).toEqual({
        type: "LOGOUT_FAILED"
      })
    })

    it("signUpStart", () => {
      const email = "pepe@gmail.com"
      const password = "passwordd"
      const firstName = "branko"
      const lastName = "ott"
      const signUpData = {
        email: "pepe@gmail.com",
        password: "passwordd",
        firstName: "branko",
        lastName: "ott"
      }
      const callback = () => null
      const resultado = signUpStart(signUpData, callback)
      expect(resultado).toEqual({
        type: "SIGN_UP_START",
        payload: {
          email,
          password,
          firstName,
          lastName,
          callback
        }
      })
    })
    it("signUpSuccess", () => {
      const user = {
        id: "324432",
        name: "Fede Osov",
        email: "fedosov@gmail.com"
      }
      const resultado = signUpSuccess(user)
      expect(resultado).toEqual({
        type: "SIGN_UP_SUCCESS",
        payload: user
      })
    })
    it("signUpFailed", () => {
      const resultado = signUpFailed()
      expect(resultado).toEqual({
        type: "SIGN_UP_FAILED"
      })
    })
  })
  describe("reducer", () => {
    it("LOGIN_START", () => {
      const initialState = {
        isLoading: false,
        user: null,
        isFailed: false
      }
      const email = "branko-96@hotmail.com"
      const password = "passw"
      const callback = () => null
      const payload = {
        email,
        password,
        callback
      }
      const resultado = authReducer(initialState, {
        type: "LOGIN_START",
        payload
      })
      expect(resultado).toEqual({ user: null, isFailed: false, isLoading: true })
    })
    it("LOGIN_SUCCESS", () => {
      const initialState = {
        isLoading: true,
        user: null,
        isFailed: false
      }
      const user = {
        id: "324432",
        name: "Fede Osov",
        email: "fedosov@gmail.com"
      }
      const resultado = authReducer(initialState, {
        type: "LOGIN_SUCCESS",
        payload: user
      })
      expect(resultado).toEqual({ user, isFailed: false, isLoading: false })
    })
    it("LOGIN_FAILED", () => {
      const initialState = {
        isLoading: true,
        user: null,
        isFailed: false
      }
      const resultado = authReducer(initialState, {
        type: "LOGIN_FAILED"
      })
      expect(resultado).toEqual({ user: null, isFailed: true, isLoading: false })
    })
    it("LOGOUT_SUCCESS", () => {
      const initialState = {
        isLoading: false,
        user: 1,
        isFailed: false
      }
      const resultado = authReducer(initialState, {
        type: "LOGOUT_SUCCESS"
      })
      expect(resultado).toEqual({ user: null, isFailed: false, isLoading: false })
    })
    it("LOGOUT_FAILED", () => {
      const initialState = {
        isLoading: false,
        user: 1,
        isFailed: false
      }
      const resultado = authReducer(initialState, {
        type: "LOGOUT_FAILED"
      })
      expect(resultado).toEqual({ user: null, isFailed: false, isLoading: false })
    })
    it("SIGN_UP_START", () => {
      const initialState = {
        isLoading: false,
        user: null,
        isFailed: false
      }
      const resultado = authReducer(initialState, {
        type: "SIGN_UP_START",
        payload: 1
      })
      expect(resultado).toEqual({ user: null, isFailed: false, isLoading: true })
    })
    it("SIGN_UP_SUCCESS", () => {
      const initialState = {
        isLoading: false,
        user: null,
        isFailed: false
      }
      const user = {
        id: "324432",
        name: "Fede Osov",
        email: "fedosov@gmail.com"
      }
      const resultado = authReducer(initialState, {
        type: "SIGN_UP_SUCCESS",
        payload: user
      })
      expect(resultado).toEqual({ user, isFailed: false, isLoading: false })
    })
    it("SIGN_UP_FAILED", () => {
      const initialState = {
        isLoading: false,
        user: null,
        isFailed: false
      }
      const resultado = authReducer(initialState, {
        type: "SIGN_UP_FAILED"
      })
      expect(resultado).toEqual({ user: null, isFailed: true, isLoading: false })
    })
    it("default", () => {
      const initialState = {
        isLoading: false,
        user: null,
        isFailed: false
      }
      const resultado = authReducer(initialState, {
        type: "NADA_EN_MI_REDUCER"
      })
      expect(resultado).toEqual(initialState)
    })
  })
})
