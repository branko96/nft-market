import {
  clearAssetSelected,
  fetchAssetsStart,
  fetchAssetsSuccess,
  selectAsset
} from "../actions/assets"
import assetsReducer from "./assets"

describe("Assets Duck", () => {
  describe("Action creators", () => {
    it("fetchAssetsStart", () => {
      const resultado = fetchAssetsStart()
      expect(resultado).toEqual({
        type: "FETCH_ASSETS_START"
      })
    })
    it("fetchAssetsSuccess", () => {
      const assets = [
        {
          id: "324432",
          imgUrl: "http://c.files.bbci.co.uk/31F0/production/_117548721_nfts2.jpg",
          name: "Nft 1",
          owner: "Fede Osov",
          price: "0.000015,20",
          likes: 20
        }
      ]
      const resultado = fetchAssetsSuccess(assets)
      expect(resultado).toEqual({
        type: "FETCH_ASSETS_SUCCESS",
        payload: assets
      })
    })
    it("selectAsset", () => {
      const asset = {
        id: "324432",
        imgUrl: "http://c.files.bbci.co.uk/31F0/production/_117548721_nfts2.jpg",
        name: "Nft 1",
        owner: "Fede Osov",
        price: "0.000015,20",
        likes: 20
      }
      const resultado = selectAsset(asset)
      expect(resultado).toEqual({
        type: "SELECT_ASSET",
        payload: asset
      })
    })
    it("clearAssetSelected", () => {
      const resultado = clearAssetSelected()
      expect(resultado).toEqual({
        type: "CLEAR_ASSET_SELECTED"
      })
    })
  })
  describe("reducer", () => {
    it("FETCH_ASSETS_SUCCESS", () => {
      const initialState = {
        assets: [],
        selectedAsset: null
      }
      const assets = [
        {
          id: "324432",
          imgUrl: "http://c.files.bbci.co.uk/31F0/production/_117548721_nfts2.jpg",
          name: "Nft 1",
          owner: "Fede Osov",
          price: "0.000015,20",
          likes: 20
        }
      ]
      const resultado = assetsReducer(initialState, {
        type: "FETCH_ASSETS_SUCCESS",
        payload: assets
      })
      expect(resultado).toEqual({ assets: assets, selectedAsset: null })
    })
    it("SELECT_ASSET", () => {
      const asset = {
        id: "324432",
        imgUrl: "http://c.files.bbci.co.uk/31F0/production/_117548721_nfts2.jpg",
        name: "Nft 1",
        owner: "Fede Osov",
        price: "0.000015,20",
        likes: 20
      }
      const initialState = {
        assets: [],
        selectedAsset: null
      }
      const resultado = assetsReducer(initialState, {
        type: "SELECT_ASSET",
        payload: asset
      })
      expect(resultado).toEqual({ assets: [], selectedAsset: asset })
    })
    it("CLEAR_ASSET_SELECTED", () => {
      const asset = {
        id: "324432",
        imgUrl: "http://c.files.bbci.co.uk/31F0/production/_117548721_nfts2.jpg",
        name: "Nft 1",
        owner: "Fede Osov",
        price: "0.000015,20",
        likes: 20
      }
      const initialState = {
        assets: [],
        selectedAsset: asset
      }
      const resultado = assetsReducer(initialState, {
        type: "CLEAR_ASSET_SELECTED",
        payload: asset
      })
      expect(resultado).toEqual({ assets: [], selectedAsset: null })
    })
    it("default", () => {
      const initialState = {
        assets: [],
        selectedAsset: null
      }
      const resultado = assetsReducer(initialState, {
        type: "NADA_EN_MI_REDUCER"
      })
      expect(resultado).toEqual(initialState)
    })
  })
})
