gnome-terminal --tab \
  --working-directory ~/workspaces/iddeadevs/nft-market/ \
  -t "React" \
  -- /bin/bash -c "endyarnstart() { exec bash; }; trap endyarnstart INT; yarn start;"

gnome-terminal --tab \
  --working-directory ~/workspaces/iddeadevs/nft-market-signraw/ \
  -t "Node" \
  -- /bin/bash -c "endnpmstart() { exec bash; }; trap endnpmstart INT; npm start;"

gnome-terminal --tab \
  -t "Rails" \
  --working-directory ~/workspaces/iddeadevs/nft-market-api/ \
  -- /bin/bash -c 'rails s; bash'

gnome-terminal --tab \
  -t "Certificates" \
  --working-directory ~/workspaces/iddeadevs/nft-market-api/ \
  -- /bin/bash -c 'rails firebase:certificates:force_request; bash'
